from django.shortcuts import render, redirect
from projects.models import Project, ProjectForm
from django.contrib.auth.models import User
from tasks.models import Task


# Create your views here.
def list_projects(request):
    # Ensure the user has logged in
    if not request.user.is_authenticated:
        return redirect("/accounts/login/")

    # Get all of the instances of the Project model
    projects = []
    for i, project in enumerate(Project.objects.filter(owner=request.user)):
        projects.append((project, i + 1))

    return render(request, "projects.html", {"projects": projects})


# View the detail of one project
def project_detail(request, id):
    # Ensure the user has logged in
    if not request.user.is_authenticated:
        return redirect("/accounts/login/")

    # Get the project
    projects = list(Project.objects.filter(owner=request.user))
    project = projects[id - 1]

    # Get all tasks of this project
    tasks = list(Task.objects.filter(project=project))

    return render(request, "detail.html", {"tasks": tasks, "project": project})


# Create a project
def create_project(request):
    if request.method == "GET":
        if not request.user.is_authenticated:
            return redirect("/accounts/login/")

        # Show the create form
        form = ProjectForm()
        return render(request, "create.html", {"create_form": form})

    elif request.method == "POST":
        # Create a project
        idx = int(request.POST.get("owner")) - 1
        Project.objects.create(
            name=request.POST.get("name"),
            description=request.POST.get("description"),
            owner=User.objects.all()[idx],
        )
        return redirect("/projects/")
