from django.urls import path
import projects.views as views

urlpatterns = [
    path("", views.list_projects, name="list_projects"),
    path("<int:id>/", views.project_detail, name="show_project"),
    path("create/", views.create_project, name="create_project"),
]
