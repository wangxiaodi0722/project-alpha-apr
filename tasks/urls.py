from django.urls import path
import tasks.views as views

urlpatterns = [
    path("create/", views.create_task, name="create_task"),
    path("mine/", views.mine, name="show_my_tasks"),
]
