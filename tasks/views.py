from django.shortcuts import render, redirect
from tasks.models import TaskForm, Task
from projects.models import Project
from django.contrib.auth.models import User


# Create your views here.
# Create a task
def create_task(request):
    if request.method == "GET":
        if not request.user.is_authenticated:
            return redirect("/accounts/login/")

        # Show the create form
        form = TaskForm()
        return render(request, "create_task.html", {"create_form": form})

    elif request.method == "POST":
        # Create a project
        assignee_idx = int(request.POST.get("assignee")) - 1
        project_idx = int(request.POST.get("project")) - 1
        Task.objects.create(
            name=request.POST.get("name"),
            start_date=request.POST.get("start_date"),
            due_date=request.POST.get("due_date"),
            project=Project.objects.all()[project_idx],
            assignee=User.objects.all()[assignee_idx],
        )
        return redirect("/projects/")


# Show my tasks
def mine(request):
    if not request.user.is_authenticated:
        return redirect("/accounts/login/")

    # Get all tasks assigned to this user
    tasks = []
    for i, task in enumerate(Task.objects.filter(assignee=request.user)):
        tasks.append(task)

    return render(request, "mine.html", {"tasks": tasks})
