from django.shortcuts import render, redirect
import accounts.models as models
from django.contrib.auth import authenticate
import django.contrib.auth as auth
from django.http import HttpResponse
from django.contrib.auth.models import User


# Create your views here.
def login(request):
    if request.method == "GET":
        # if request.user.is_authenticated:
        #     return redirect('/projects/')

        # Show the login form
        form = models.LoginForm()
        return render(request, "accounts/login.html", {"login_form": form})

    elif request.method == "POST":
        # Try to log the person in

        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)
        if user is None:
            return HttpResponse("Authentication failure.")

        auth.login(request, user)
        # If the person is successfully logged in, it is redirected to the list of projects
        return redirect("/projects/")


def logout(request):
    auth.logout(request)
    return redirect("/accounts/login/")


def signup(request):
    if request.method == "GET":
        # Show the signup form
        form = models.SignupForm()
        return render(request, "accounts/signup.html", {"signup_form": form})

    elif request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        password_confirmation = request.POST.get("password_confirmation")
        if password != password_confirmation:
            # Two passwords are not the same
            return HttpResponse("Two passwords are not the same.")

        if len(User.objects.filter(username=username)) > 0:
            return HttpResponse("The user exists.")

        user = User.objects.create_user(username=username, password=password)
        auth.login(request, user)
        # redirect to projects
        return redirect("/projects/")
